﻿namespace MetroSet_UI.Enums
{
    public enum ScrollOrientate
    {
        /// <summary>
        /// Sets the the scrollbar on horizontal orientation.
        /// </summary>
        Horizontal,

        /// <summary>
        /// Sets the the scrollbar on vertical orientation.
        /// </summary>
        Vertical
    }
}